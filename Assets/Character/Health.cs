using UnityEngine;
using UnityEngine.AI;

public class Health : MonoBehaviour
{
    [SerializeField] private float _maxHealth;
    [SerializeField] private float _currentHealth;

    [SerializeField] private Animator _animator;

    void Start()
    {
        _currentHealth = _maxHealth;
    }

    public void TakeDamage(float damage)
    {
        _currentHealth -= damage;

        if (_currentHealth <= 0)
        {
            Debug.Log("Die");
            Die();
        }
    }

    public void TakeCriticalDamage()
    {
        _currentHealth = 0f;
        Debug.Log("Critical Die");
        Die();
    }

    private void Die()
    {
        _animator.SetBool("isDeath", true);
        Destroy(gameObject, 5f);
    }
}
