using UnityEngine;

public enum HitboxArea
{
    Critical,
    Other,
}

[RequireComponent(typeof(Collider))]
public class Hitbox : MonoBehaviour
{
    public HitboxArea HitboxArea;
    [SerializeField] private Health _health;

    public void TakeHit(float damage)
    {
        if (HitboxArea == HitboxArea.Critical)
        {
            _health.TakeCriticalDamage();
            return;
        }
        _health.TakeDamage(damage);
    }
}
