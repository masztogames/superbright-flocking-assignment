using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    [CreateAssetMenu(menuName = "Flock/Behavior/Cohesion")]

    public class CohesionBehavior : FilderedBehavior
    {
        private Vector3 _currentPosition;
        [SerializeField] private float _movementSmooth = 0.5f;

        public override Vector3 CalculateTarget(FlockUnit unit, List<Transform> transforms, Flock flock)
        {
            if(transforms.Count == 0)
            {
                return Vector3.zero;
            }

            Vector3 cohesionPosition = Vector3.zero;
            List<Transform> filteredTransforms = (Filter == null) ? transforms : Filter.Filter(unit, transforms);
            foreach(Transform item in filteredTransforms)
            {
                cohesionPosition += item.position;
            }
            cohesionPosition /= transforms.Count;

            cohesionPosition -= unit.transform.position;
            cohesionPosition = Vector3.SmoothDamp(unit.transform.position, cohesionPosition, ref _currentPosition, _movementSmooth);
            return cohesionPosition;
        }

    }
}
