using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    [System.Serializable]
    public struct BehaviorStruct
    {
        public BaseBehavior Behavior;
        [Range(0.01f, 10f)]
        public float weight;
    }

    [CreateAssetMenu(menuName = "Flock/Behavior/Combined")]
    public class CombinedBehaviors : BaseBehavior
    {

        public BehaviorStruct[] Behaviors;

        public override Vector3 CalculateTarget(FlockUnit unit, List<Transform> transforms, Flock flock)
        {
            if (Behaviors.Length == 0)
            {
                Debug.Log("Miss  behavior in " + name, this);
                return Vector3.zero;
            }

            Vector3 position = Vector3.zero;

            for (int i = 0; i < Behaviors.Length; i++)
            {
                Vector3 partialPosition = Behaviors[i].Behavior.CalculateTarget(unit, transforms, flock) * Behaviors[i].weight;

                if (partialPosition != Vector3.zero)
                {
                    if (partialPosition.sqrMagnitude > Behaviors[i].weight * Behaviors[i].weight)
                    {
                        partialPosition.Normalize();
                        partialPosition *= Behaviors[i].weight;
                    }

                    position += partialPosition;
                }
            }
            return position;
        }
    }
}
