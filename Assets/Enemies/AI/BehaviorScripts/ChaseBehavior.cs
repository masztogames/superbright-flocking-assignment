using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    [CreateAssetMenu(menuName = "Flock/Behavior/Chase")]
    public class ChaseBehavior : FilderedBehavior
    {
        public override Vector3 CalculateTarget(FlockUnit unit, List<Transform> transforms, Flock flock)
        {
            if (transforms.Count == 0)
            {
                return Vector3.zero;
            }

            Vector3 chasePosition = Vector3.zero;

            List<Transform> filteredTransforms = (Filter == null) ? transforms : Filter.Filter(unit, transforms);
            foreach (Transform item in filteredTransforms)
            {
                chasePosition += flock.PlayerPosition;
            }

            chasePosition /= transforms.Count;
            return chasePosition;
        }
    }
}
