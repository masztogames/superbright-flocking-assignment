using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    [CreateAssetMenu(menuName = "Flock/Behavior/Avoidance")]
    public class AvoidanceBehavior : FilderedBehavior
    {
        public override Vector3 CalculateTarget(FlockUnit unit, List<Transform> transforms, Flock flock)
        {
            if (transforms.Count == 0)
            {
                return Vector3.zero;
            }

            Vector3 avoidencePosition = Vector3.zero;
            int numerObjectToAvoid = 0;
            List<Transform> filteredTransforms = (Filter == null) ? transforms : Filter.Filter(unit, transforms);
            foreach (Transform item in filteredTransforms)
            {
                if (Vector3.SqrMagnitude(item.position - unit.transform.position) < flock.SquareAvoidanceRadius)
                {
                    numerObjectToAvoid++;
                    avoidencePosition += unit.transform.position - item.position;
                }
            }
            avoidencePosition = numerObjectToAvoid > 0 ? avoidencePosition / numerObjectToAvoid : avoidencePosition;

            return avoidencePosition;
        }
    }
}
