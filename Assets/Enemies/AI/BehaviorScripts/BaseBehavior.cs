using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    public abstract class BaseBehavior : ScriptableObject
    {
        public abstract Vector3 CalculateTarget(FlockUnit unit, List<Transform> transforms, Flock flock);
    }
}