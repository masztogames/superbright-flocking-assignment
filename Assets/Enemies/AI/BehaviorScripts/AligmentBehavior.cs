using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    [CreateAssetMenu(menuName = "Flock/Behavior/Alignment")]
    public class AligmentBehavior : FilderedBehavior
    {
        public override Vector3 CalculateTarget(FlockUnit unit, List<Transform> transforms, Flock flock)
        {
            if (transforms.Count == 0)
            {
                return unit.transform.position;
            }

            Vector3 aligmentPosition = Vector3.zero;
            List<Transform> filteredTransforms = (Filter == null) ? transforms : Filter.Filter(unit, transforms);
            foreach (Transform item in filteredTransforms)
            {
                aligmentPosition += item.position;
            }
            
            return aligmentPosition /= transforms.Count; 
        }
    }
}
