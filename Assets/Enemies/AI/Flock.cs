using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    public class Flock : MonoBehaviour
    {
        public FlockUnit _unitPrefab;
        private List<FlockUnit> _units = new List<FlockUnit>();

        [SerializeField] private BaseBehavior _behavior;

        [SerializeField] private int _unitsToSpawn;
        [SerializeField] private float _detectingRadius;
        [SerializeField] private int _minDistanceFromSpawner;
        [SerializeField] private int _maxDistanceFromSpawner;

        [SerializeField] private float _playerPositionRefreshingTime = 1.5f;
        private ObjectPool _unitsPool;

        private Vector3 _playerPosition;
        public Vector3 PlayerPosition { get { return _playerPosition; } }

        [Range(1f, 10f)]
        public float _neighborRadius = 1.5f;
        [Range(0f, 1f)]
        public float _avoidanceRadiusMultiplier = 0.5f;

        float _squareNeighborRadius;
        float _squareAvoidanceRadius;
        public float SquareAvoidanceRadius { get { return _squareAvoidanceRadius; } }

        private int RandomDistance() => Random.Range(_minDistanceFromSpawner, _maxDistanceFromSpawner);
        private int RandomPosition() => Random.Range(-RandomDistance(), RandomDistance());
        private Vector3 GetRandomPosition()
        {
            return new Vector3(RandomPosition(), 0.1f, RandomPosition());
        }

        private void Awake()
        {
            _unitsPool = GetComponentInChildren<ObjectPool>();
        }

        void Start()
        {
            SpawnUnits(_unitsToSpawn);

            _squareNeighborRadius = _neighborRadius * _neighborRadius;
            _squareAvoidanceRadius = _squareNeighborRadius * _avoidanceRadiusMultiplier * _avoidanceRadiusMultiplier;
            StartCoroutine(PlayerPositionTimer());
        }

        void Update()
        {
            foreach (FlockUnit unit in _units)
            {
                List<Transform> nearbyUnits = GetNerbyObjects(unit);

                Vector3 target = _behavior.CalculateTarget(unit, nearbyUnits, this) + _playerPosition;

                if (unit.gameObject.activeInHierarchy)
                {
                    unit.MoveTo(target);
                }
            }
        }

        private List<Transform> GetNerbyObjects(FlockUnit unit)
        {
            List<Transform> transforms = new List<Transform>();

            Collider[] transformsColiders = Physics.OverlapSphere(unit.transform.position, _detectingRadius);
            foreach (Collider c in transformsColiders)
            {
                if (c != unit.UnitCollider)
                {
                    transforms.Add(c.transform);
                }
            }
            return transforms;
        }

        public void SpawnUnits(int value)
        {
            for (int i = 0; i < value; i++)
            {
                FlockUnit unit = _unitsPool.GetPooledObject();
                unit.gameObject.SetActive(true);
                unit.name = "Unit " + i;
                unit.Initialize(this);
                _units.Add(unit);
                unit.transform.position = GetRandomPosition();
            }
        }

        IEnumerator PlayerPositionTimer()
        {
            _playerPosition = GetPlayerPosition();
            yield return new WaitForSeconds(_playerPositionRefreshingTime);
            StartCoroutine(PlayerPositionTimer());
        }

        private Vector3 GetPlayerPosition()
        {
            GameObject player = GameObject.Find("Player");
            return player.transform.position;
        }

    }
}
