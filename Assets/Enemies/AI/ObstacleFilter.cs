using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    [CreateAssetMenu(menuName = "Flock/Filter/Obstacle")]
    public class ObstacleFilter : ContextFilter
{
        public LayerMask ObstacleLayer;

        public override List<Transform> Filter(FlockUnit unit, List<Transform> transforms)
        {
            List<Transform> filteredTransforms = new List<Transform>();
            foreach(Transform item in transforms)
            {
                if (ObstacleLayer == ( ObstacleLayer | 1 << item.gameObject.layer))
                {
                    filteredTransforms.Add(item);
                }
            }

            return filteredTransforms;
        }
}
}
