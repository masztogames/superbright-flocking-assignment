using System.Collections.Generic;
using UnityEngine;

namespace TJ
{
    public abstract class ContextFilter : ScriptableObject
    {
        public abstract List<Transform> Filter(FlockUnit unit, List<Transform> transforms);
    }
}
