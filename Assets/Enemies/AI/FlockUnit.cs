using UnityEngine;
using UnityEngine.AI;

namespace TJ
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class FlockUnit : MonoBehaviour
    {
        private NavMeshAgent _navMeshAgent;
        private Flock _unitFlock;
        public Flock UnitFlock { get { return _unitFlock; } }

        private Collider _unitCollider;
        public Collider UnitCollider { get { return _unitCollider; } }
        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public void MoveTo(Vector3 target)
        {
            _navMeshAgent.SetDestination(target);
        }

        public void Initialize(Flock flock)
        {
            _unitFlock = flock;
        }
    }
}
