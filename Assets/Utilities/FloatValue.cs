using UnityEngine;

[CreateAssetMenu(menuName ="Variables/FloatValue")]
public class FloatValue : ScriptableObject
{
    public float Value;

    public void AddValue(float value)
    {
        Value += value;
    }

    public void AddPercent(float value)
    {
        value += 100;

        Value = (Value * value) / 100;
    }
}
