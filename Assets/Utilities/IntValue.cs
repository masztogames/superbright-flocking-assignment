using UnityEngine;

[CreateAssetMenu(menuName = "Variables/IntValue")]
public class IntValue : ScriptableObject
{
    public int Value;

    public void AddValue(int value)
    {
        Value += value;
    }
}
