using System.IO;
using UnityEngine;

namespace TJ.BLS.Test
{
    public class DataManager : MonoBehaviour
    {
        [SerializeField] private GameData _data;
        private string _fileName = "player.txt";

        public GameData Data => _data;

        private void Awake()
        {
            Load();
        }

        public void Load()
        {
            _data = new GameData();
            string json = ReadFromFIle(_fileName);
            JsonUtility.FromJsonOverwrite(json, _data);
        }

        public void Save()
        {
            string json = JsonUtility.ToJson(_data);
            WriteToFile(_fileName, json);
        }

        private void WriteToFile(string fileName, string json)
        {
            string path = GetFilePath(fileName);
            FileStream fileStream = new FileStream(path, FileMode.Create);

            using (StreamWriter writer = new StreamWriter(fileStream))
            {
                writer.Write(json);
            }
        }

        private string ReadFromFIle(string fileName)
        {
            string path = GetFilePath(fileName);
            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    string json = reader.ReadToEnd();
                    return json;
                }
            }
            else
            {
                Debug.LogWarning("File not found");
            }

            return "Success";
        }

        private string GetFilePath(string fileName)
        {
            return Application.persistentDataPath + "/" + fileName;
        }

        private void OnApplicationPause(bool pause)
        {
            if (pause)
            {
                Save();
            }
        }
    }
}
