using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private int _amount;

    private List<GameObject> _pooledObjects = new List<GameObject>();

    private static ObjectPool _instance;
    public static ObjectPool Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("ObjectPool instance is null");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        for (int i = 0; i < _amount; i++)
        {
            GameObject pooledObject = Instantiate(_prefab);
            pooledObject.transform.parent = transform;
            pooledObject.SetActive(false);
            _pooledObjects.Add(pooledObject);
        }
    }

    public GameObject GetPooledObject()
    {
        foreach (GameObject pooledObject in _pooledObjects)
        {
            if (!pooledObject.activeInHierarchy)
            {
                return pooledObject;
            }
        }
        return null;
    }
}
