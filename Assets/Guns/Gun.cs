using System.Collections;
using TMPro;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [Header("Gun stats")]
    [SerializeField] private float _damage;
    [SerializeField] private float _spread = 0.02f;
    [SerializeField] private float _aimSpreed = 0.01f;
    [SerializeField] private float _shootingRange;
    [SerializeField] private int _maxAmmo;
    [SerializeField] private float _reloadTime = 1f;
    [SerializeField] private float _fireRate = 15f;
    private float _nextTimeToFire = 0f;
    private int _currentAmmo;
    private bool _reloading;

    [Header("References")]
    [SerializeField] private Camera _fpsCam;
    [SerializeField] private Transform _attackPoint;
    [SerializeField] private RaycastHit _rayHit;
    [SerializeField] private LayerMask _enemyLayer;
    [SerializeField] private ParticleSystem _muzzleFlash;
    [SerializeField] private GameObject _bulletHoleGraphic;
    [SerializeField] private TextMeshProUGUI _text;

    [Header("Recoil values")]
    [SerializeField] private float _recoilX;
    [SerializeField] private float _recoilY;
    [SerializeField] private float _recoilZ;

    [Header("Recoil values when aiming")]
    [SerializeField] private bool _canAiming;
    [SerializeField] private float _aimRecoilX;
    [SerializeField] private float _aimRecoilY;
    [SerializeField] private float _aimRecoilZ;
    private bool _isAiming = false;

    [Header("Other values")]
    [SerializeField] private float _snapiness;
    [SerializeField] private float _returnSpeed;

    private Recoil _recoil;
    private Crosshair _crosshair;
    private Ray _ray;

    private void Awake()
    {
        _currentAmmo = _maxAmmo;
        _recoil = FindObjectOfType<Recoil>();
        _crosshair = FindObjectOfType<Crosshair>();
    }

    private void Start()
    {
        UpdateUI();
        _recoil.SetCurrentGunVariables(_returnSpeed, _snapiness, _recoilX, _recoilY, _recoilZ, _aimRecoilX, _aimRecoilY, _aimRecoilZ);
    }

    private void Update()
    {
        ShootingBehaviour();

        if (_canAiming)
        {
            ZoomBehaviour();
        }
    }

    private void UpdateUI()
    {
        _text.SetText(_currentAmmo + " / " + _maxAmmo);
    }

    private void ZoomBehaviour()
    {
        _isAiming = Input.GetMouseButton(1) && !_reloading;
    }

    private void ShootingBehaviour()
    {
        _crosshair.IsGrowing = Input.GetButton("Fire1") && !_reloading;

        if (!_reloading)
        {
            if (Input.GetButton("Fire1") && Time.time >= _nextTimeToFire)
            {
                _nextTimeToFire = Time.time + 1f / _fireRate;
                Shoot();
            }
        }
    }

    private void Shoot()
    {
        //_muzzleFlash.Play();

        Vector3 randomSpread = _isAiming ? new Vector3(RandomAimSpreed(), RandomAimSpreed(), 0) : new Vector3(RandomSpreed(), RandomSpreed(), 0);
        Vector3 direction = _fpsCam.transform.forward + randomSpread;

        if (Physics.Raycast(_fpsCam.transform.position, direction, out _rayHit, _shootingRange, _enemyLayer))
        {
            Debug.Log(_rayHit.collider.name);

            _rayHit.collider.GetComponent<Hitbox>()?.TakeHit(_damage);
        }

        _recoil.RecoilFire(_isAiming);

        // GameObject bulletHole = Instantiate(_bulletHoleGraphic, _rayHit.point, Quaternion.identity, _rayHit.transform);
        // bulletHole.transform.rotation = Quaternion.LookRotation(_rayHit.normal);
        _currentAmmo--;
        UpdateUI();

        if (_currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }
    }

    IEnumerator Reload()
    {
        _reloading = true;
        yield return new WaitForSeconds(_reloadTime);
        _currentAmmo = _maxAmmo;
        UpdateUI();
        _reloading = false;
    }

    private float RandomSpreed() => Random.Range(-_spread, _spread);
    private float RandomAimSpreed() => Random.Range(-_aimSpreed, _aimSpreed);
}