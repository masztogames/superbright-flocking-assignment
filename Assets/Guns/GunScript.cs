using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunScript : MonoBehaviour
{
    [Header("Shooting")]
    [SerializeField] private Transform _attackPoint;
    [SerializeField] private float _attackRange;
    [SerializeField] private float _spread;
    [SerializeField] private float _shootForce;
    [Tooltip("Using only when need bounce bullets")]
    [SerializeField] private float _upwardForce;
    [SerializeField] private float _fireRate;
    private float _nextTimeToFire = 0f;
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private Camera _fpsCamera;
    [SerializeField] private Sprite _crosshair;

    [Header("Ammo")]
    [SerializeField] private float _reloadTime;
    [SerializeField] private int _maxCapSize;
    [SerializeField] private int _ammo;
    private int _currentCap;

    [Header("Recoil")] // TODO: add SO variables for recoil stats
    [SerializeField] private float _returnSpeed;
    [SerializeField] private float _snapiness;
    [SerializeField] private float _recoilZ;
    [SerializeField] private float _recoilY;
    [SerializeField] private float _recoilX;

    private float RandomSpread() => Random.Range(-_spread, _spread);
    private ObjectPool _pool;
    private Recoil _recoil;

    private void Awake()
    {
        _pool = GetComponent<ObjectPool>();
        _recoil = GetComponentInParent<Recoil>();
    }

    void Start()
    {
       // _recoil.SetCurrentGunVariables(_returnSpeed, _snapiness, _recoilX, _recoilY, _recoilZ);

        GameObject.Find("Crosshair").GetComponent<Image>().sprite = _crosshair;
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= _nextTimeToFire)
        {
            _nextTimeToFire = Time.time + 1f / _fireRate;
            Shoot();
        }
    }

    void Shoot()
    {

        Ray ray = _fpsCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        Vector3 targetPoint;

        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
        }
        else
        {
            targetPoint = ray.GetPoint(_attackRange);
        }

        //_recoil.RecoilFire();

        Vector3 directionWithoutSpread = targetPoint - _attackPoint.position;
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(RandomSpread(), RandomSpread(), 0);

        GameObject bullet = _pool.GetPooledObject();
        bullet.SetActive(true);
        bullet.transform.parent = null;
        bullet.transform.position = _attackPoint.position;

        bullet.transform.forward = directionWithSpread.normalized;

        bullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * _shootForce, ForceMode.Impulse);

        bullet.GetComponent<Rigidbody>().AddForce(_fpsCamera.transform.up * _upwardForce, ForceMode.Impulse);
    }
}
