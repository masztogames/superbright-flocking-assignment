using System;
using UnityEngine;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour
{
    private RectTransform _rectTransform;
    private float _currentSize;
    [SerializeField] private float _restingSize;
    [SerializeField] private float _maxSize;
    [SerializeField] private float _speed;
    [SerializeField] private bool _isGrowing;
    public bool IsGrowing { set { _isGrowing = value; } }

    private Image _image;
    [SerializeField] private Image _dot;

    void Start()
    {
        Cursor.visible = false;
        _rectTransform = GetComponent<RectTransform>();
        _image = GetComponent<Image>();
    }

    void Update()
    {
        //_isGrowing = Input.GetButton("Fire1");
        _currentSize = _isGrowing ? Mathf.Lerp(_currentSize, _maxSize, Time.deltaTime * _speed) : Mathf.Lerp(_currentSize, _restingSize, Time.deltaTime * _speed);
        _rectTransform.sizeDelta = new Vector2(_currentSize, _currentSize);
    }
}
