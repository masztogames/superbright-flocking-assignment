using UnityEngine;

public class Recoil : MonoBehaviour
{
    #region Hidden variables
    private Vector3 _currentRotation;
    private Vector3 _targetRotation;

    // [SerializeField] private GunSwitcher _gunSwitcher;

    private float _returnSpeed;
    private float _snapiness;
    private float _aimRecoilY;
    private float _aimRecoilX;
    private float _aimRecoilZ;
    private float _recoilZ;
    private float _recoilY;
    private float _recoilX;
    #endregion

    void Awake()
    {
        //_gunSwitcher = FindObjectOfType<GunSwitcher>();
    }

    void Update()
    {
        _targetRotation = Vector3.Lerp(_targetRotation, Vector3.zero, _returnSpeed * Time.deltaTime);
        _currentRotation = Vector3.Slerp(_currentRotation, _targetRotation, _snapiness * Time.fixedDeltaTime);

        transform.localRotation = Quaternion.Euler(_currentRotation);
    }

    public void RecoilFire(bool isAiming)
    {
        if (isAiming)
        {
            _targetRotation += new Vector3(_aimRecoilX, Random.Range(-_aimRecoilY, _aimRecoilY), Random.Range(-_aimRecoilZ, _aimRecoilZ));
            return;
        }
        _targetRotation += new Vector3(_recoilX, Random.Range(-_recoilY, _recoilY), Random.Range(-_recoilZ, _recoilZ));
    }

    public void SetCurrentGunVariables(float returnSpeed, float snapiness, float recoilX, float recoilY, float recoilZ, float aimRecoilX, float aimRecoilY, float aimRecoilZ)
    {
        _returnSpeed = returnSpeed;
        _snapiness = snapiness;
        _aimRecoilX = aimRecoilX;
        _aimRecoilY = aimRecoilY;
        _aimRecoilZ = aimRecoilZ;
        _recoilX = recoilX;
        _recoilY = recoilY;
        _recoilZ = recoilZ;
    }
}
