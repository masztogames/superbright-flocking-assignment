using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Mouse")]
    [SerializeField] [Range(0.0f, 0.5f)] float _mouseSmoothTime = 0.03f;
    [SerializeField] bool _cursorLock = true;
    [SerializeField] bool _cursorVisible = true;
    [SerializeField] float _mouseSensitivity = 3.5f;
    private float _cameraCap;
    private Vector2 _currentMouseDelta;
    private Vector2 _currentMouseDeltaVelocity;
    private Transform _playerCamera;

    [Header("Movement")]
    [SerializeField] float _speed = 6.0f;
    [SerializeField] [Range(0.0f, 0.5f)] float _moveSmoothTime = 0.3f;
    private CharacterController _controller;
    private Vector2 _currentDir;
    private Vector2 _currentDirVelocity;
    private Vector3 _velocity;

    [Header("Jump")]
    [SerializeField] float _gravity = -30f;
    [SerializeField] float _jumpHeight = 6f;
    [SerializeField] Transform _groundCheck;
    [SerializeField] LayerMask _ground;
    private float _velocityY;
    private bool _isGrounded;

    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _playerCamera = GetComponentInChildren<Camera>().transform;

        Cursor.lockState = _cursorLock ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = _cursorVisible;
    }

    void Update()
    {
        MouseInput();
        MoveInput();
    }

    void MouseInput()
    {
        Vector2 targetMouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        _currentMouseDelta = Vector2.SmoothDamp(_currentMouseDelta, targetMouseDelta, ref _currentMouseDeltaVelocity, _mouseSmoothTime);

        _cameraCap -= _currentMouseDelta.y * _mouseSensitivity;

        _cameraCap = Mathf.Clamp(_cameraCap, -90.0f, 90.0f);

        _playerCamera.localEulerAngles = Vector3.right * _cameraCap;

        transform.Rotate(Vector3.up * _currentMouseDelta.x * _mouseSensitivity);
    }

    void MoveInput()
    {
        _isGrounded = Physics.CheckSphere(_groundCheck.position, 0.2f, _ground);

        Vector2 targetDir = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        targetDir.Normalize();

        _currentDir = Vector2.SmoothDamp(_currentDir, targetDir, ref _currentDirVelocity, _moveSmoothTime);

        _velocityY += _gravity * 2f * Time.deltaTime;

        Vector3 velocity = (transform.forward * _currentDir.y + transform.right * _currentDir.x) * _speed + Vector3.up * _velocityY;

        _controller.Move(velocity * Time.deltaTime);

        if (_isGrounded && Input.GetButtonDown("Jump"))
        {
            _velocityY = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
        }

        if (_isGrounded! && _controller.velocity.y < -1f)
        {
            _velocityY = -8f;
        }
    }
}